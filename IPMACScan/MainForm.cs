﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Net;
using System.Threading;

namespace IPMACScan
{
    public partial class MainForm : Form
    {
        public class IPMac
        {
            public string IP { set; get; }
            public string MAC { set; get; }
        }

        public class Data
        {
            public string IPStart { set; get; }
            public string IPEnd { set; get; }
            public string MAC { set; get; }
        }
        private CancellationTokenSource _CancellationTokenSource = new CancellationTokenSource();

        private string _FileName = "";

        private List<IPMac> _IPMACList = new List<IPMac>();
        public MainForm()
        {
            InitializeComponent();
        }

        private void _Scan_Click(object sender, EventArgs e)
        {
            string[] ip_start = _IPStart.Text.Split('.');
            string[] ip_end = _IPEnd.Text.Split('.');
            Int32[] ip_s = new Int32[4];
            Int32[] ip_e = new Int32[4];
            int i;
            if (_Scan.Text == "Stop")
            {
                _CancellationTokenSource.Cancel();
                return;
            }
            if (ip_start.Length != 4 || ip_end.Length != 4)
            {
                MessageBox.Show("IPの記述が不正です。", "エラー", MessageBoxButtons.OK);
                return;
            }
            for (i = 0; i < ip_s.Length; ++i)
            {
                Int32 n;
                if (!Int32.TryParse(ip_start[i], out n))
                {
                    MessageBox.Show("IPの記述が不正です。", "エラー", MessageBoxButtons.OK);
                    return;
                }
                ip_s[i] = n;
            }
            for (i = 0; i < ip_e.Length; ++i)
            {
                Int32 n;
                if (!Int32.TryParse(ip_end[i], out n))
                {
                    MessageBox.Show("IPの記述が不正です。", "エラー", MessageBoxButtons.OK);
                    return;
                }
                ip_e[i] = n;
            }

            _IPMACList.Clear();
            _Scan.Text = "Stop";

            var task = Task.Factory.StartNew(() =>
            {
                for (i = 1; i < ip_s.Length; ++i)
                {
                    if (ip_s[i - 1] != ip_e[i - 1])
                    {
                        ip_e[i] = 255;
                    }
                }

                for (int i0 = ip_s[0]; i0 <= ip_e[0]; ++i0)
                {
                    for (int i1 = ip_s[1]; i1 <= ip_e[1]; ++i1)
                    {
                        for (int i2 = ip_s[2]; i2 <= ip_e[2]; ++i2)
                        {
                            for (int i3 = ip_s[3]; i3 <= ip_e[3]; ++i3)
                            {
                                if (_CancellationTokenSource.IsCancellationRequested)
                                {
                                    return;
                                }
                                var ip = String.Format("{0}.{1}.{2}.{3}", i0, i1, i2, i3);
                                _Status.Text = "IP:" + ip + "をスキャン中";
                                var mac = GetMacAddress(ip);
                                if (mac != null)
                                {
                                    _IPMACList.Add(new IPMac() { IP = ip, MAC = mac });
                                    if (_MAC.Text.Replace(':', '-').ToUpper() == mac.ToUpper())
                                    {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }
                return;
            }, _CancellationTokenSource.Token);

            task.ContinueWith(t =>
            {
                if (_MAC.Text != "")
                {
                    _Result.DataSource = _IPMACList.Where(w => w.MAC == _MAC.Text.Replace(':', '-').ToUpper()).ToList();
                }
                else
                {
                    _Result.DataSource = _IPMACList;
                }
                _CancellationTokenSource = new CancellationTokenSource();
                _Scan.Text = "Scan";
                _Status.Text = "スキャン終了";
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        [DllImport("iphlpapi.dll", ExactSpelling = true)]
        private static extern int SendARP(int dstIp, int srcIp, byte[] mac, ref int macLen);


        private string GetMacAddress(string ipAddress)
        {
            IPAddress dst = IPAddress.Parse(ipAddress);

            var mac = new byte[6];
            var macLen = mac.Length;
            if (SendARP((int)dst.Address, 0, mac, ref macLen) == 0)
            {
                return string.Format("{0:X2}-{1:X2}-{2:X2}-{3:X2}-{4:X2}-{5:X2}", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
            }
            return null;
        }

        private void 新規作成ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _FileName = "";
            _IPStart.Text = "";
            _IPEnd.Text = "";
            _MAC.Text = "";
        }

        private void 開くToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_OpenFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.Xml.Serialization.XmlSerializer serializer;
                serializer = new System.Xml.Serialization.XmlSerializer(typeof(Data));
                using (System.IO.StreamReader sr = new System.IO.StreamReader(_OpenFileDialog.FileName, new System.Text.UTF8Encoding(false)))
                {
                    var data = (Data)serializer.Deserialize(sr);
                    _FileName = _OpenFileDialog.FileName;
                    _IPStart.Text = data.IPStart;
                    _IPEnd.Text = data.IPEnd;
                    _MAC.Text = data.MAC;
                }
            }
        }

        private void 保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_FileName == "")
            {
                名前を付けて保存ToolStripMenuItem_Click(sender, e);
                return;
            }
            System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(Data));
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(_FileName, false, new System.Text.UTF8Encoding(false)))
            {
                s.Serialize(sw, new Data() { IPStart = _IPStart.Text, IPEnd = _IPEnd.Text, MAC = _MAC.Text });
            }
        }

        private void 名前を付けて保存ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                System.Xml.Serialization.XmlSerializer s = new System.Xml.Serialization.XmlSerializer(typeof(Data));
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(_SaveFileDialog.FileName, false, new System.Text.UTF8Encoding(false)))
                {
                    _FileName = _SaveFileDialog.FileName;
                    s.Serialize(sw, new Data() { IPStart = _IPStart.Text, IPEnd = _IPEnd.Text, MAC = _MAC.Text });
                }
            }
        }

        private void 終了ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string hostname = Dns.GetHostName();

            // ホスト名からIPアドレスを取得する
            IPAddress[] adrList = Dns.GetHostAddresses(hostname);
            foreach (IPAddress address in adrList)
            {
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    string addr_str = address.ToString();
                    if (addr_str.IndexOf(".") > 0 && !addr_str.StartsWith("127."))
                    {
                        string[] ip = addr_str.Split('.');
                        _IPStart.Text = String.Format("{0}.{1}.{2}.2", ip[0], ip[1], ip[2]);
                        _IPEnd.Text = String.Format("{0}.{1}.{2}.254", ip[0], ip[1], ip[2]);
                        break;
                    }
                }
            }
        }
    }
}
